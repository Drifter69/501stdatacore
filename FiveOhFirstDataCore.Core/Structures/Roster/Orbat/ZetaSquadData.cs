using FiveOhFirstDataCore.Data.Account;

namespace FiveOhFirstDataCore.Data.Structures.Roster
{
    public class ZetaSquadData : IAssignable<Trooper>
    {
        public Trooper SquadLeader { get; set; }
        public Trooper Leader { get; set; }
        public Trooper RT { get; set; }
        public Trooper Medic { get; set; }
        public Trooper[] Medics { get; private set; } = new Trooper[2];
        public Trooper[] Troopers { get; private set; } = new Trooper[10];
        public Trooper ARC {get; set; }

        public void Assign(Trooper item)
        {
            if (item.Team is null)
            {
                switch (item.Role)
                {
                    case Role.Lead:
                        SquadLeader = item;
                        break;
                    case Role.Subordinate:
                        Leader = item;
                        break;
                    default:
                        for (int j = 0; j < Medics.Length; j++)
                        {
                            if (Medics[j] is null)
                            {
                                Medics[j] = item;
                                break;
                            }
                        }
                        break;
                }    
            }
            else
            {
                switch (item.Role)
                {
                    case Role.RTO:
                        RT = item;
                        break;
                    case Role.ARC:
                        ARC = item;
                        break;
                    default:
                        for (int i = 0; i < Troopers.Length; i++)
                        {
                            if (Troopers[i] is null)
                            {
                                Troopers[i] = item;
                                break;
                            }
                        }
                        break;
                }
            }    
        }
    }
}